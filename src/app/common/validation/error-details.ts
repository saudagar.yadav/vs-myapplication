export interface ErrorDetail{
    errorStatus : boolean; 
    htmlIconId : string;
    htmlInputId : string;
    title : string;
}
