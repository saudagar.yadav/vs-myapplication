import { Injectable } from "@angular/core";
import { Observable, ReplaySubject } from "rxjs";
import { filter } from "rxjs/operators";
import { Action } from "./dispatcher";

@Injectable()
export class BaseStore {

    public subject: ReplaySubject<string> = new ReplaySubject(0);
    public noInternetConnection: boolean;

    public emitChange(status: string): void {
        this.subject.next(status);
    }

    public emitError<T>(error: T): void {
        this.subject.error(error);
    }

    public addListener(inputStatus: string): Observable<string> {
        return this.subject.pipe(filter((status: string) => status === inputStatus));
    }

    public emitStatus = (action: Action): void => {
        this.emitChange(action.actionType);
    }

    public setNoInternetFlagAndEmitStatus = (action: Action): void => {
        this.noInternetConnection = action.data.noInternetConnection;
        this.emitStatus(action);
    }

    public isInternetConnectionLost(): boolean {
        return this.noInternetConnection;
    }
}