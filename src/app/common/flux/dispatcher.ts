import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { filter } from "rxjs/operators";

export interface Action {
    actionType: string;
    data?: any;
}

@Injectable()
export class Dispatcher {
    private dispatcher: Subject<Action> = new Subject();

    public dispatch(action: Action): void {
        this.dispatcher.next(action);
    }

    public registerForAction(actionType: string): Observable<Action> {
        return this.dispatcher.pipe(filter((action: Action) => action.actionType === actionType));
    }

}