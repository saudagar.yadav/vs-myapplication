import { Subscription } from "rxjs";

export class BaseSubscriptionService {
    private subscriptionList: Subscription[] = [];
    constructor() { }
    public addSubscription(subscription: Subscription): void {
        this.subscriptionList.push(subscription);
    }
}