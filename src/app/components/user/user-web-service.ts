import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { UserDetails } from './user-details';

const httpOption = {
  headers: new HttpHeaders({'Content-type':"application/json"})
};

@Injectable({
  providedIn: 'root'
})
export class UserWebService {
  constructor(private http:HttpClient) { }
  public register(userData : UserDetails) {
    let body = JSON.stringify(userData);
    return this.http.post('/server/api/v2/user/register', body, httpOption);
  }
  
  public loginUser(userData){
    let body = JSON.stringify(userData);
    return this.http.post('/server/api/v2/user/login', body, httpOption);
  }
}
