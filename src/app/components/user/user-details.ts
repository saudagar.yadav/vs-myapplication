export interface UserDetails {
    name: string;
    emailId: string;
    contactNo: string;
    password: string;
}

export interface RegistrationResponse {
    status: string;
    errorData: Array<ErrorData>;
}

export interface ErrorData {
    errorMsg: string;
    errorCode: number;
}

export interface LoginUserDetail {
    userName: string;
    password: string;
}
