import { Injectable } from "@angular/core";
import { Dispatcher } from "../../common/flux/dispatcher";
import { UserWebService } from "./user-web-service";
import { UserDetails, RegistrationResponse } from './user-details';
import { UserActionConstants } from "./user-action-constants";

@Injectable()
export class UserActionCreator {
    constructor(private dispatcher: Dispatcher,
        private userWebService: UserWebService) {
    }
    public register(userData : UserDetails) {
        
        this.dispatcher.dispatch({
            actionType: UserActionConstants.REGISTER_USER_SUCCESS,
            data: "test"
        });
        
        /*return this.userWebService.register(userData).subscribe((successdata: RegistrationResponse) => {
            if (successdata.Status){
                this.dispatcher.dispatch({
                    actionType: UserActionConstants.REGISTER_USER_SUCCESS,
                    data: successdata
                });
            } else {
                this.dispatcher.dispatch({
                    actionType: UserActionConstants.REGISTER_USER_FAILURE,
                    data: successdata
                });
            }
        }, (error: Error) => console.log("error"));    */
    }   
}