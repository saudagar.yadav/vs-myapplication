import { Component, OnInit } from '@angular/core'; 
import * as jQuery from 'jquery';
import { Router} from '@angular/router';

import { UserWebService } from '../user-web-service';
import { UserHelperService } from '../user-helper-service';3
import { ValidationService } from '../../../services/validation.service';
import { UserDetails, RegistrationResponse, ErrorData } from './../user-details';
import { ErrorDetail } from './../../../common/validation/error-details';
import { ValidationConstants } from './../../../common/validation/validation-constants';

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  private errorDetails: ErrorDetail;
  public name: string;
  public emailId: string;
  public contactNo: string;
  public password: string;
  public confirmPassword: string;
  constructor( 
    private ValidationService: ValidationService,
    private userWebService: UserWebService,
    private userHelperService: UserHelperService,
    private router: Router) {}

  ngOnInit() {
    jQuery("#mainBodyId").addClass("mainBody");
  }

  public register():void{
    if(this.validate()){
      const userData : UserDetails = {
        name: this.name,
        emailId: this.emailId,
        contactNo:this.contactNo,
        password: this.password
      };
      
      this.userWebService.register(userData).subscribe((responsedata: RegistrationResponse) => {
        if (responsedata.status){
          alert("success");
          this.router.navigate(["\login"]);      
        } else {
          this.userHelperService.showErrorOccuredForRegistraion(responsedata.errorData);
        }
      }, (error: Error) => console.log("error"));
    }
  }
  
  public validate() : boolean{
    let validationFlag : boolean = this.validateName();
    validationFlag = this.validateEmailId() && validationFlag;
    validationFlag = this.validateConatactNo() && validationFlag;
    validationFlag = this.validatePassword() && validationFlag;
    validationFlag = this.validateConfirmPassword() && validationFlag;
    return validationFlag;
  }

  public validateName() : boolean {
    this.errorDetails = this.userHelperService.setDefaultErrorDetails("#signupInputNameId", "#signupIconNameId");
    if (this.ValidationService.isEmpty(this.name)){
      this.errorDetails.title = ValidationConstants.BLANK_ERROR;
      this.errorDetails.errorStatus = true;
    } else if (this.ValidationService.isInvalidTextAllowsSpace(this.name)){
      this.errorDetails.title = "Invalid Name."
      this.errorDetails.errorStatus = true;
    }
    this.userHelperService.applyValidationCss(this.errorDetails);
    return !this.errorDetails.errorStatus;
  }

  public validateEmailId() : boolean {
    this.errorDetails = this.userHelperService.setDefaultErrorDetails("#signupInputEmailId", "#signupIconEmailId");
    if (this.ValidationService.isEmpty(this.emailId)){
      this.errorDetails.title = ValidationConstants.BLANK_ERROR;
      this.errorDetails.errorStatus = true;
    } else if (this.ValidationService.isInvalidEmailId(this.emailId)){
      this.errorDetails.title = "Invalid Email Id."
      this.errorDetails.errorStatus = true;
    }
    
    this.userHelperService.applyValidationCss(this.errorDetails);
    return !this.errorDetails.errorStatus;
  }
  
  public validateConatactNo() : boolean {
    this.errorDetails = this.userHelperService.setDefaultErrorDetails("#signupInputConatactNoId", "#signupIconConatactNoId");
    if (this.ValidationService.isEmpty(this.contactNo)){
      this.errorDetails.title = ValidationConstants.BLANK_ERROR;
      this.errorDetails.errorStatus = true;
    } else if (this.ValidationService.isInvalidPhoneNumber(this.contactNo)){
      this.errorDetails.title = "Invalid Contact no."
      this.errorDetails.errorStatus = true;
    }
    
    this.userHelperService.applyValidationCss(this.errorDetails);
    return !this.errorDetails.errorStatus;
  }
  
  public validatePassword() : boolean {
    this.errorDetails = this.userHelperService.setDefaultErrorDetails("#signupInputPasswordId", "#signupIconPasswordId");
    if (this.ValidationService.isEmpty(this.password)){
      this.errorDetails.title = ValidationConstants.BLANK_ERROR;
      this.errorDetails.errorStatus = true;
    } else if (6 > this.password.length || this.password.length >15){
      this.errorDetails.title = "Password length should be between 6 to 15.";
      this.errorDetails.errorStatus = true;
    }
    
    this.userHelperService.applyValidationCss(this.errorDetails);
    return !this.errorDetails.errorStatus;
  }
  
  public validateConfirmPassword() : boolean {
    this.errorDetails = this.userHelperService.setDefaultErrorDetails("#signupInputConfirmPasswordId", "#signupIconConfirmPasswordId");
    if (this.ValidationService.isEmpty(this.confirmPassword)){
      this.errorDetails.title = ValidationConstants.BLANK_ERROR;
      this.errorDetails.errorStatus = true;
    } else if (this.password !== this.confirmPassword){
      this.errorDetails.title = "Please make sure password match.";
      this.errorDetails.errorStatus = true;
    }
    this.userHelperService.applyValidationCss(this.errorDetails);
    return !this.errorDetails.errorStatus;
  }
}

