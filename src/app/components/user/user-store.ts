import { Injectable } from "@angular/core";

import { BaseStore } from "../../common/flux/base-store";
import { Action, Dispatcher } from "../../common/flux/dispatcher";

import { UserActionConstants } from "./user-action-constants";

@Injectable()
export class UserStore extends BaseStore {
	private pgConfigErrorMsg: string;
    constructor(private dispatcher: Dispatcher) {
        super();
        this.dispatcher.registerForAction(UserActionConstants.REGISTER_USER_SUCCESS).subscribe(this.emitStatus);
    }
    
}