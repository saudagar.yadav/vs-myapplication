import { Component, OnInit } from '@angular/core';
import { WebService } from '../../../services/web.service';
import { ValidationService } from '../../../services/validation.service';
import * as jQuery from 'jquery';
import { LoginUserDetail, ErrorData } from  '../user-details';
import { ErrorDetail } from './../../../common/validation/error-details';
import { ValidationConstants } from './../../../common/validation/validation-constants';
import { UserWebService } from '../user-web-service';
import { UserHelperService } from '../user-helper-service';
import { Router} from '@angular/router';
 
@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private errorDetails: ErrorDetail;
  public userName: string;
  public password: string;
  public loginData: any;

  constructor( 
    private validationService: ValidationService,
    private userHelperService: UserHelperService,
    private userWebService: UserWebService,
    private router: Router) {}

  ngOnInit() {
    jQuery("#mainBodyId").addClass("mainBody");
  }

  public login(): void{
    let validateData = this.validate();
      if(validateData) { 
        const userData: LoginUserDetail = {
        userName: this.userName, 
        password: this.password
      };
      
      this.userWebService.loginUser(userData).subscribe((responsedata: any) => {
        if (responsedata.status){
          alert("success");
          this.router.navigate(["\hiking"]);      
        } else {
          this.userHelperService.showErrorOccuredForRegistraion(responsedata.errorData);
        }
      }, (error: Error) => console.log("login error"));
    }
  }

  public validate(){ 
    let validationFlag:boolean = this.validateUsername();
    validationFlag = this.validatePwd() && validationFlag;  
    return validationFlag;
  }

  public validateUsername(){
    this.errorDetails = this.userHelperService.setDefaultErrorDetails("#loginInputUsernameId", "#loginIconUsernameId");
    if (this.validationService.isEmpty(this.userName)){
      this.errorDetails.title = ValidationConstants.BLANK_ERROR;
      this.errorDetails.errorStatus = true;
    }
    this.userHelperService.applyValidationCss(this.errorDetails);
    return !this.errorDetails.errorStatus;
  }

  public validatePwd(){
    this.errorDetails = this.userHelperService.setDefaultErrorDetails("#loginInputPwdId", "#loginIconPwdId");
    if (this.validationService.isEmpty(this.password)){
      this.errorDetails.title = ValidationConstants.BLANK_ERROR;
      this.errorDetails.errorStatus = true;
    } else if (6 > this.password.length || this.password.length >15){
      this.errorDetails.title = "Password length should be between 6 to 15.";
      this.errorDetails.errorStatus = true;
    }
    this.userHelperService.applyValidationCss(this.errorDetails);
    return !this.errorDetails.errorStatus;
  }
}

