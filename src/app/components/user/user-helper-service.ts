import * as jQuery from 'jquery';
import { ErrorData } from './user-details';
import { ErrorDetail } from '../../common/validation/error-details';

export class UserHelperService {
    private errorDetails: ErrorDetail;
    public showErrorOccuredForRegistraion(errorDataArr: Array<ErrorData>) : void {
      for (let i = 0; i < errorDataArr.length; i++) {
        const errorData: ErrorData= errorDataArr[i];
        if (errorData.errorCode === 1) {
          this.setDefaultErrorDetails("#loginInputUsernameId", "#loginIconUsernameId");
        } else if (errorData.errorCode === 2) {
          this.setDefaultErrorDetails("#loginInputPwdId", "#loginIconPwdId");
        } else if (errorData.errorCode === 3) {
          this.setDefaultErrorDetails("#signupInputEmailId", "#signupIconEmailId");
        } else if (errorData.errorCode === 4) {
          this.setDefaultErrorDetails("#signupInputConatactNoId", "#signupIconConatactNoId");
        }
        this.errorDetails.title = errorData.errorMsg;
        this.errorDetails.errorStatus = true;
        this.applyValidationCss(this.errorDetails);
      }
    }

    public applyValidationCss(errorDetails : ErrorDetail) : void{
        if(errorDetails.errorStatus){
            jQuery(errorDetails.htmlIconId).addClass("iconError").css("cursor","pointer").attr("title", errorDetails.title);
            jQuery(errorDetails.htmlInputId).addClass("inputError");
        } else {
            jQuery(errorDetails.htmlIconId).removeClass("iconError").removeAttr("title");
            jQuery(errorDetails.htmlInputId).removeClass("inputError");
        }
    }
    
    public setDefaultErrorDetails(htmlInputId: string, htmlIconId: string) : ErrorDetail {
        return this.errorDetails = {
            errorStatus : false,
            htmlIconId : htmlIconId,
            htmlInputId : htmlInputId,
            title : "",
        };
    }
}